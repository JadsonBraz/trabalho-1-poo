# Get the GCC preinstalled image from Docker Hub
FROM gcc:9.4

# Copy the current folder which contains C++ source code to the Docker image under /usr/src
COPY ./src /app

# Specify the working directory
WORKDIR /app

# Use GCC to compile the *.cpp source file
RUN g++ -o tp1 *.cpp

# Run the program output from the previous step
CMD ["./tp1"]