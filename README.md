# Trabalho 1 POO - Sistema de gestão de salas de aula

Este trabalho possui como objetivo o desenvolvimento de um sistema de gestão de salas de aula.
No sistema deverá ser possível realizar o cadastro de alunos, de professores, de gestores, de cursos e das salas de aula.

## Começando

### GCC
Para executar o programa, basta compilar o código dentro da pasta src:

	g++ -o main *.cpp

E executar o programa:

	./main

### Docker
Outra opção é executar o programa a partir de uma imagem docker. Para construir a imagem, basta executar o comando:

	docker build -t poo .

E para executar o programa, basta executar o comando:

	docker run -it poo

### Makefile
Uma última forma utilizada para compilar e executar o programa é através do makefile. Para compilar o programa, basta executar o comando:

	make

E para executar o programa, basta executar o comando:

	./TP1
