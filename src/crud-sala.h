#ifndef __CRUD_SALA_H_
#define __CRUD_SALA_H_

#include <iostream>
#include <vector>
#include "sala.h"

class CrudSala
{
	vector<Sala> &salas;	// vetor de todas as salas cadastradas

protected:
	int capacidade, codigo;	// dados utilizados no input de dados do usuario

public:
	CrudSala(vector<Sala> &salas);	// crud de um parametro: salas cadastradas
	~CrudSala();	// destrutor
	bool cadastrar();	// funcao que cadastra uma nova sala
};

#endif
