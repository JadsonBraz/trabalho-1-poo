#include "relatorio.h"
#include <fstream>
#include <iomanip>

// construtor de 4 parametros (alunos, professores, cursos e salas)
Relatorio::Relatorio(vector<Aluno> &alunos, vector<Professor> &professores, vector<Curso> &cursos, vector<Sala> &salas)
    : alunos(alunos), professores(professores), cursos(cursos), salas(salas) {}

Relatorio::~Relatorio() {} // destrutor

// relatorio de alunos
void Relatorio::relatorioAlunos()
{
    // Imprimir relatório no terminal
    cout << setw(30) << right << "Nome"
         << setw(20) << right << "CPF"
         << setw(15) << right << "Matricula"
         //  << setw(20) << right << "Curso"
         << setw(20) << right << "Telefone"
         << endl;
    for (Aluno aluno : alunos) // para cada aluno na lista de alunos
    {
        cout << setw(30) << right << aluno.getNome()
             << setw(20) << right << aluno.getCpf()
             << setw(15) << right << aluno.getMatricula()
             //  << setw(20) << right << aluno.getCodCurso()
             << setw(20) << right << aluno.getTelefone()
             << endl;
    }

    // Gerar .txt do relatório
    ofstream relatorio_aluno_;
    relatorio_aluno_.open("relatorios/Relatorio_Aluno.txt"); // abrir arquivo
    if (relatorio_aluno_.is_open())                          // se o arquivo estiver aberto
    {
        relatorio_aluno_ << setw(30) << right << "Nome" // imprimir no arquivo
                         << setw(20) << right << "CPF"
                         << setw(15) << right << "Matricula"
                         //  << setw(10) << right << "Curso"
                         << setw(20) << right << "Telefone"
                         << endl;
        for (Aluno aluno : alunos) // para cada aluno na lista de alunos
        {
            relatorio_aluno_ << setw(30) << right << aluno.getNome() // imprimir no arquivo
                             << setw(20) << right << aluno.getCpf()
                             << setw(15) << right << aluno.getMatricula()
                             //  << setw(10) << right << aluno.getCodCurso()
                             << setw(20) << right << aluno.getTelefone()
                             << endl;
        }
        relatorio_aluno_.close(); // fechar arquivo
        cin.get();                // pausar o programa
    }
}

// relatorio de professores
void Relatorio::relatorioProfessores()
{
    // Imprimir relatório no terminal
    cout << setw(30) << right << "Nome"
         << setw(20) << right << "CPF"
         << setw(20) << right << "Telefone"
         << endl;
    for (Professor professor : professores) // para cada professor na lista de professores
    {
        cout << setw(30) << right << professor.getNome() // imprimir no terminal
             << setw(20) << right << professor.getCpf()
             << setw(20) << right << professor.getTelefone()
             << endl;
    }

    // Gerar .txt do relatório
    ofstream relatorio_professor_;                                   // criar arquivo
    relatorio_professor_.open("relatorios/Relatorio_Professor.txt"); // abrir arquivo
    if (relatorio_professor_.is_open())                              // se o arquivo estiver aberto
    {
        relatorio_professor_ << setw(30) << right << "Nome" // imprimir no arquivo
                             << setw(20) << right << "CPF"
                             << setw(20) << right << "Telefone"
                             << endl;
        for (Professor professor : professores) // para cada professor na lista de professores
        {
            relatorio_professor_ << setw(30) << right << professor.getNome() // imprimir no arquivo
                                 << setw(20) << right << professor.getCpf()
                                 << setw(20) << right << professor.getTelefone()
                                 << endl;
        }
        relatorio_professor_.close(); // fechar arquivo
        cin.get();                    // pausar o programa
    }
}

// relatorio de cursos
void Relatorio::relatorioCursos()
{
    vector<string> diasDaSemana = {"Segunda", "Terça", "Quarta", "Quinta", "Sexta"};
    // Imprimir relatório no terminal
    cout << setw(30) << right << "Nome"
         << setw(20) << right << "Professor"
         << setw(10) << right << "Alunos"
         << setw(50) << right << "Dias da Semana"
         << endl;
    for (Curso curso : cursos) // para cada curso na lista de cursos
    {
        cout << setw(30) << right << curso.getNome() // imprimir no terminal
             << setw(20) << right << curso.getProfessor().getCpf()
             << setw(10) << right << curso.getAlunosMatriculados().size()
             << setw(10);
        for (int dia : curso.getDiasDaSemana())             // para cada dia na lista de dias
            cout << setw(30) << right << dia; // imprimir no terminal
        cout << endl;
    }

    // Gerar .txt do relatório
    ofstream relatorio_curso_;                               // criar arquivo
    relatorio_curso_.open("relatorios/Relatorio_Curso.txt"); // abrir arquivo
    if (relatorio_curso_.is_open())                          // se o arquivo estiver aberto
    {
        relatorio_curso_ << setw(30) << right << "Nome" // imprimir no arquivo
                         << setw(20) << right << "Professor"
                         << setw(10) << right << "Qtd Alunos"
                         << setw(50) << right << "Dias da Semana"
                         << endl;
        for (Curso curso : cursos) // para cada curso na lista de cursos
        {
            relatorio_curso_ << setw(30) << right << curso.getNome() // imprimir no arquivo
                             << setw(20) << right << curso.getProfessor().getCpf()
                             << setw(10) << right << curso.getAlunosMatriculados().size()
                             << setw(50);
            for (int dia : curso.getDiasDaSemana())                         // para cada dia na lista de dias
                relatorio_curso_ << setw(30) << right << diasDaSemana[dia]; // imprimir no arquivo
            relatorio_curso_ << endl;                                       // pular linha
        }
        relatorio_curso_.close(); // fechar arquivo
        cin.get();                // pausar o programa
    }
}

// relatorio de salas
void Relatorio::relatorioSalas()
{
    // Imprimir relatório no terminal
    cout << setw(7) << right << "Sala" // imprimir no terminal
         << setw(20) << right << "Capacidade"
         << setw(20) << right << "Curso"
         << endl;
    for (Sala sala : salas)
    {
        cout << setw(7) << sala.getCodigo();              // imprimir no terminal
        for (Curso curso : sala.getCursos())              // para cada curso na lista de cursos
            cout << setw(20) << right << curso.getNome(); // imprimir no terminal
        cout << setw(15) << sala.getCapacidade()          // imprimir no terminal
             << endl;
    }

    ofstream relatorio_sala_;                              // criar arquivo
    relatorio_sala_.open("relatorios/Relatorio_Sala.txt"); // abrir arquivo
    if (relatorio_sala_.is_open())                         // se o arquivo estiver aberto
    {
        relatorio_sala_ << setw(7) << right << "Sala"
                        << setw(20) << right << "Capacidade"
                        << setw(20) << right << "Curso"
                        << endl;
        for (Sala sala : salas)
        {
            relatorio_sala_ << setw(7) << sala.getCodigo(); // imprimir no arquivo
            for (Curso curso : sala.getCursos())
                relatorio_sala_ << setw(20) << right << curso.getNome(); // imprimir no arquivo
            relatorio_sala_ << setw(15) << sala.getCapacidade()          // imprimir no arquivo
                            << endl;
        }
        relatorio_sala_.close(); // fechar arquivo
        cin.get();               // pausar o programa
    }
}