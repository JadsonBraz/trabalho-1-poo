#include "aluno.h"

// construtor de cinco parametros: nome, telefone, cpf, matricula e codigo do curso:
Aluno::Aluno(string nome, string telefone, string cpf, int matricula) : Pessoa(nome, telefone, cpf), matricula(matricula) {}
// destrutor
Aluno::~Aluno() {}

int Aluno::getMatricula() const // funcao que retorna a matricula do aluno (constante: somente leitura)
{
	return matricula;
}

void Aluno::setMatricula(int matricula) // funcao que altera a matricula do aluno
{
	this->matricula = matricula;
}
