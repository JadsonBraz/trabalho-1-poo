#include "menu.h"
#include "dados.h"
#include <fstream>
#include <iomanip>

int main()
{
	// fabricas de objetos
	AlocarCursos alocarCursos(salas, cursos);
	CrudAluno crudAluno(alunos, cursos);
	CrudProfessor crudProfessor(professores, cursos);
	CrudCurso crudCurso(cursos, professores);
	CrudGestor crudGestor(gestores);
	CrudSala crudSala(salas);
	Relatorio relatorio(alunos, professores, cursos, salas);

	system("clear"); // limpa a tela

	cout << "Bem vindo ao sistema de gestão de salas de aula!" << endl
		 << endl;
	for (int dia : curso1.getDiasDaSemana())
	{
		cout << dia;
	}
	alocarCursos.alocar();																		   // chama o metodo alocar da classe AlocarCursos
	Menu menu(crudAluno, crudProfessor, crudGestor, crudCurso, crudSala, alocarCursos, relatorio); // instancia o menu

	menu.login(); // chama o metodo login do menu: o usuario pode ser aluno, professor ou gestor

	return 0;
}