#ifndef __RELATORIO_H_
#define __RELATORIO_H_

#include <vector>
#include <iostream>

#include "aluno.h"
#include "professor.h"
#include "curso.h"
#include "sala.h"
#include "gestor.h"

using namespace std;

class Relatorio
{
    vector<Aluno> &alunos;
    vector<Professor> &professores;
    vector<Curso> &cursos;
    vector<Sala> &salas;

public:
    // construtor de 4 parametros (alunos, professores, cursos e salas)
    Relatorio(vector<Aluno> &alunos, vector<Professor> &professores, vector<Curso> &cursos, vector<Sala> &salas);
    ~Relatorio();                // destrutor
    void relatorioAlunos();      // relatorio de alunos
    void relatorioProfessores(); // relatorio de professores
    void relatorioCursos();      // relatorio de cursos
    void relatorioSalas();       // relatorio de salas
};

#endif