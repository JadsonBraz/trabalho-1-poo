#ifndef __CRUD_GESTOR_H_
#define __CRUD_GESTOR_H_

#include <iostream>
#include <vector>
#include "gestor.h"
using namespace std;

class CrudGestor
{
	vector<Gestor> gestores;	// vetor de todos os gestores cadastrados

protected:
	// dados utilizados no input de dados do usuario
	string usuario, senha, nome, cpf, telefone;

public:
	// crud de um parametro: gestores cadastrados
	CrudGestor(vector<Gestor> &gestores);
	~CrudGestor();	// destrutor
	bool autenticar();	// funcao que autentica um gestor
	void cadastrar();	// funcao que cadastra um novo gestor
};

#endif