#ifndef __PROFESSOR_H_
#define __PROFESSOR_H_

#include "pessoa.h"

class Professor : public Pessoa
{
	int codCurso;	// codigo do curso que o professor leciona

public:
	Professor(string nome, string telefone, string cpf, int codCurso);	// construtor de 4 parametros (nome, telefone, cpf e codCurso)
	Professor(string nome, string telefone, string cpf);	// construtor de 3 parametros (nome, telefone e cpf)
	Professor();	// construtor de 0 parametros
	~Professor();									// destrutor
	// getters e setters
	int getCodCurso();							
	void setCodCurso(int codCurso);
};

#endif