#include "crud-professor.h"

// construtor de dois parametros: professores e cursos cadastrados
CrudProfessor::CrudProfessor(vector<Professor> &professores, vector<Curso> &cursos) : professores(professores), cursos(cursos) {}
// destrutor
CrudProfessor::~CrudProfessor() {}

// funcao que cria um novo professor
bool CrudProfessor::criar()
{

	cout << "Cadastrar professor" << endl;
	cout << "Digite o nome do professor: ";
	cin >> nome;
	cout << "Digite o telefone do professor: ";
	cin >> telefone;
	cout << "Digite o CPF do professor: ";
	cin >> cpf;
	// verifica se o professor já existe
	for (Professor professor : professores)
	{
		if (professor.getCpf() == cpf)
		{
			professorExiste = true; // professor já existe
			cout << "Professor já cadastrado!" << endl;
			return false; // retorna false para indicar que o professor não foi criado
		}
	}

	// cria professor e adiciona ao vetor de professores
	Professor professor(nome, telefone, cpf);
	professores.push_back(professor); // adiciona o professor ao vetor de professores
	cout << "Professor cadastrado com sucesso!" << endl;
	return true; // retorna true para indicar que o professor foi criado
}

// funcao que pesquisa professores cadastrados
void CrudProfessor::ler()
{
	cout << "Consultar professor" << endl;

	cout << "Digite o CPF do professor: ";
	cin >> cpf;
	// verifica se o professor existe
	for (int i = 0; i < (int)professores.size(); i++)
	{
		if (professores[i].getCpf() == cpf)
		{
			professorExiste = true;
			cout << "Nome: " << professores[i].getNome() << endl;
			cout << "Telefone: " << professores[i].getTelefone() << endl;
			break;
		}
	}

	if (!professorExiste)
	{
		cout << "Professor não cadastrado!" << endl; // professor não existe
	}
	professorExiste = false; // reseta a variavel para a proxima consulta
}

// funcao que edita os dados de um professor
void CrudProfessor::editar()
{
	cout << "Atualizar professor" << endl;

	cout << "Digite o CPF do professor: ";
	cin >> cpf;

	for (int i = 0; i < (int)professores.size(); i++)
	{
		if (professores[i].getCpf() == cpf)
		{
			professorExiste = true; // professor existe
			cout << "Digite o novo nome do professor: ";
			cin >> nome;
			cout << "Digite o novo telefone do professor: ";
			cin >> telefone;
			professores[i].setNome(nome);
			professores[i].setTelefone(telefone);
			cout << "Professor atualizado com sucesso!" << endl;
			break;
		}
	}

	if (!professorExiste)
	{
		cout << "Professor não cadastrado!" << endl;
	}
	professorExiste = false; // reseta a variavel para a proxima consulta
}

// funcao que remove um professor
void CrudProfessor::remover()
{
	cout << "Remover professor" << endl;

	cout << "Digite o CPF do professor: ";
	cin >> cpf;

	for (int i = 0; i < (int)professores.size(); i++)
	{
		if (professores[i].getCpf() == cpf)
		{
			professorExiste = true;
			for (int j = 0; j < (int)cursos.size(); j++)
			{
				if (cursos[j].getProfessor().getCpf() == cpf)	// verifica se o professor é professor de algum curso
				{
					cursos[j].setProfessor(Professor());	// remove o professor do curso
				}
			}
			professores.erase(professores.begin() + i);	// remove o professor do vetor de professores
			cout << "Professor removido com sucesso!" << endl;	
			break;
		}
	}

	if (!professorExiste)
	{
		cout << "Professor não cadastrado!" << endl;
	}
	professorExiste = false;
}