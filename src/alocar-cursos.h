#ifndef __ALOCAR_CURSOS_H_
#define __ALOCAR_CURSOS_H_

#include <iostream>	// biblioteca padrao de entrada e saida de dados
#include <algorithm>	
#include "sala.h"	// biblioteca de salas de aula
#include "curso.h"	// biblioteca de cursos

using namespace std;	// padrao de nomes

class AlocarCursos
{
	vector<Sala> salas;	  // vetor de todas as salas de aula cadastradas
	vector<Curso> cursos; // vetor de todos os cursos cadastrados

public:
	AlocarCursos(vector<Sala> salas, vector<Curso> cursos); // construtor de dois parametros: salas e cursos cadastrados
	AlocarCursos();											// construtor vazio
	~AlocarCursos();										// destrutor
	void alocar();											// funcao que aloca os cursos nas salas
	void verAlocacao();										// funcao que mostra a alocacao de cursos nas salas
};

#endif