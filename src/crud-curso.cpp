#include "crud-curso.h"

// construtor de um parametro: vetor de cursos
CrudCurso::CrudCurso(vector<Curso> &cursos, vector<Professor> &professores) : cursos(cursos), professores(professores) {}
// destrutor
CrudCurso::~CrudCurso() {}

// funcao que cria um novo curso
bool CrudCurso::criar()
{
	bool professorExiste = false;
	cout << "Cadastrar curso" << endl;
	cout << "Digite o nome do curso: ";
	cin >> nome;
	for (Curso curso : cursos)
	{
		if (curso.getNome() == nome)
		{
			cout << "Curso ja existe" << endl;
			return false;
		}
	}

	cout << "Digite o cpf do professor: ";
	cin >> cpfProfessor;

	for (Professor prof : professores)
	{
		if (prof.getCpf() == cpfProfessor)
			professor = prof;
		professorExiste = true;
		break;
	}

	if (professorExiste)
	{
		Curso curso(nome, professor); // cria curso
		cursos.push_back(curso); // adiciona curso ao vetor de cursos
		cout << "Curso cadastrado com sucesso!" << endl;
		return true; // retorna true para indicar que o curso foi criado
	}
	else
	{
		cout << "Professor nao encontrado" << endl;
		Curso curso(nome); // cria curso
		cursos.push_back(curso); // adiciona curso ao vetor de cursos
		cout << "Curso cadastrado com sucesso!" << endl;
		return true; // retorna true para indicar que o curso foi criado
	}
}

// funcao que pesquisa cursos cadastrados
void CrudCurso::ler()
{
	cout << "Consultar curso" << endl;

	cout << "Digite o nome do curso (0 mostra todos): ";
	cin >> nome;

	for (Curso curso : cursos)
	{
		if (curso.getNome() == nome || nome == "0")
		{
			cursoExiste = true;
			cout << "Nome: " << curso.getNome() << endl;
			cout << "Cpf do professor: " << curso.getProfessor().getCpf() << endl;
		}
	}
	if (!cursoExiste)
		cout << "Curso nao encontrado" << endl;
	cursoExiste = false;
}

// funcao que edita um curso
void CrudCurso::editar()
{
	cout << "Editar curso" << endl;

	cout << "Digite o nome do curso: ";
	cin >> nome;

	for (Curso curso : cursos)
	{
		if (curso.getNome() == nome)
		{
			cursoExiste = true;
			cout << "Digite o novo nome do curso: ";
			cin >> nome;
			cout << "Digite o novo cpf do professor: ";
			cin >> cpfProfessor;

			for(Professor prof : professores) {
				if(prof.getCpf() == cpfProfessor)
					professor = prof;
			}

			curso.setNome(nome);
			curso.setProfessor(professor);
			cout << "Curso editado com sucesso!" << endl;
			break;
		}
	}
	if (!cursoExiste)
		cout << "Curso nao encontrado" << endl;
	cursoExiste = false;
}

// funcao que remove um curso
void CrudCurso::remover()
{
	cout << "Remover curso" << endl;

	cout << "Digite o nome do curso: ";
	cin >> nome;

	for (int i = 0; i < (int)cursos.size(); i++)
	{
		if (cursos[i].getNome() == nome)
		{
			cursoExiste = true;
			cursos.erase(cursos.begin() + i);
			cout << "Curso deletado com sucesso!" << endl;
			break;
		}
	}
	if (!cursoExiste)
		cout << "Curso nao encontrado" << endl;
	cursoExiste = false;
}