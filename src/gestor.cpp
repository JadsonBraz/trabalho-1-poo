#include "gestor.h"

// construtor de 5 parametros (nome, telefone, cpf, usuario e senha)
Gestor::Gestor(string nome, string telefone, string cpf, string usuario, string senha) : Pessoa(nome, telefone, cpf)
{
	this->usuario = usuario;
	this->senha = senha;
}

Gestor::~Gestor() {} // destrutor

string Gestor::getUsuario() const // getters e setters
{
	return usuario;
}

bool Gestor::autenticar(string usuario, string senha) const // autentica o usuario e a senha
{
	return this->usuario == usuario && this->senha == senha;
}
