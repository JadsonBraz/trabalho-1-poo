#include "crud-aluno.h"

// construtor de dois parametros: vetor de alunos e vetor de cursos
CrudAluno::CrudAluno(vector<Aluno> &alunos, vector<Curso> &cursos) : alunos(alunos), cursos(cursos) {}
// destrutor
CrudAluno::~CrudAluno() {}

// funcao que cria um novo aluno
bool CrudAluno::criar()
{
	bool cursoExiste = false, cpfExiste = false; // variaveis de controle (flags)

	cout << "Cadastrar novo aluno" << endl;
	cout << "Digite o nome do aluno: ";
	cin >> nome;
	cout << "Digite o telefone do aluno: ";
	cin >> telefone;
	cout << "Digite o CPF do aluno: ";
	cin >> cpf;
	// verifica se o cpf ja existe
	for (Aluno aluno : alunos)
	{
		if (aluno.getCpf() == cpf)
		{
			cpfExiste = true;
			break;
		}
	}
	cout << "Digite a matricula do aluno: ";
	cin >> matricula;
	// verifica se a matricula ja existe
	for (Aluno aluno : alunos)
	{
		if (aluno.getMatricula() == matricula)
		{
			alunoExiste = true;
			break;
		}
	}

	// verifica se o cpf e a matricula nao existem, e se o curso existe
	// caso verdadeiro, cria o aluno e adiciona ao vetor de alunos
	if (!cpfExiste && cursoExiste && !alunoExiste)
	{
		// cria aluno e adiciona ao vetor de alunos
		Aluno aluno(nome, telefone, cpf, matricula);
	}
	alunoExiste = false;

	return false;
}

// funcao que pesquisa alunos cadastrados
void CrudAluno::ler()
{
	cout << "Consultar alunos" << endl;
	cout << "Digite a matricula do aluno: ";
	cin >> matricula;

	for (Aluno aluno : alunos)
	{
		if (aluno.getMatricula() == matricula)
		{
			alunoExiste = true;
			cout << "Nome: " << aluno.getNome() << endl;
			cout << "Telefone: " << aluno.getTelefone() << endl;
			cout << "Matricula: " << aluno.getMatricula() << endl;

			// busca nome do curso do aluno cadastrado
			for (Curso curso : cursos)
			{
				for (Aluno alunoMatriculado : curso.getAlunosMatriculados())
					if (alunoMatriculado.getMatricula() == aluno.getMatricula())
						cout << "Curso: " << curso.getNome() << endl;
			}
			break;
		}
	}

	if (!alunoExiste)
	{
		cout << "Aluno não encontrado!" << endl; // caso o aluno nao seja encontrado
	}
	alunoExiste = false;
}

// funcao que edita os dados de um aluno
void CrudAluno::editar()
{
	cout << "Editar aluno" << endl;
	cout << "Digite a matricula do aluno: ";
	cin >> matricula;

	for (Aluno aluno : alunos)
	{
		if (aluno.getMatricula() == matricula)
		{
			alunoExiste = true;
			cout << "Digite o nome do aluno: ";
			cin >> nome;
			cout << "Digite o telefone do aluno: ";
			cin >> telefone;
			cout << "Digite o CPF do aluno: ";
			cin >> cpf;
			cout << "Digite a matricula do aluno: ";
			cin >> matricula;

			aluno.setNome(nome);
			aluno.setTelefone(telefone);
			aluno.setCpf(cpf);
			aluno.setMatricula(matricula);
			break;
		}
	}

	if (!alunoExiste)
	{
		cout << "Aluno não encontrado!" << endl;
	}
	alunoExiste = false;
}

// funcao que remove um aluno
void CrudAluno::remover()
{
	cout << "Remover aluno" << endl;
	cout << "Digite a matricula do aluno: ";
	cin >> matricula;

	for (int i = 0; i < (int)alunos.size(); i++)
	{
		if (alunos[i].getMatricula() == matricula)
		{
			// Remover aluno do curso
			for (Curso curso : cursos)
			{
				for(Aluno alunoMatriculado : curso.getAlunosMatriculados()) {
					if(alunoMatriculado.getMatricula() == matricula)
						curso.removeAluno(alunos[i]);
				}
			}

			alunos.erase(alunos.begin() + i); // remove aluno do vetor de alunos
			alunoExiste = true;
			cout << "Aluno removido com sucesso!" << endl;
			break;
		}
	}

	if(!alunoExiste)
	{
		cout << "Aluno não encontrado!" << endl;
	}
	alunoExiste = false;
}
