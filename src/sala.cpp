#include "sala.h"

// construtor de 2 parametros (codigo e capacidade)
Sala::Sala(int codigo, int capacidade) : codigo(codigo), capacidade(capacidade) {}
Sala::~Sala() {} // destrutor

// getters
int Sala::getCapacidade() const
{
	return capacidade;
}

// adicionar codigo de curso alocado na sala
void Sala::adicionaCurso(Curso curso)
{
	cursos.push_back(curso);
}

// getters
int Sala::getCodigo() const
{
	return codigo;
}

vector<Curso> Sala::getCursos() const
{
	return cursos;
}