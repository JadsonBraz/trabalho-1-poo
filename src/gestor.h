#ifndef __GESTOR_H_
#define __GESTOR_H_

#include "pessoa.h"

class Gestor : public Pessoa
{
	string usuario, senha;	// usuario e senha do gestor

public:
	// construtor de 5 parametros (nome, telefone, cpf, usuario e senha)
	Gestor(string nome, string telefone, string cpf, string usuario, string senha);
	~Gestor();											 // destrutor
	string getUsuario() const;							 // getters e setters
	bool autenticar(string usuario, string senha) const; // autentica o usuario e a senha
};

#endif