#include "curso.h"

Curso::Curso(string nome, Professor professor) : nome(nome), professor(professor) // construtor de tres parametros (nome, codigo e cpf do professor)
{
	this->alocado = false;
}

Curso::Curso(string nome) : nome(nome) // construtor de dois parametros (nome e codigo)
{
	// this->professor = new Professor("","","");
	this->alocado = false;
}

Curso::~Curso() {} // destrutor

// getters e setters
string Curso::getNome() const
{
	return nome;
}

// getters e setters
void Curso::setNome(string nome)
{
	this->nome = nome;
}

// getters e setters
Professor Curso::getProfessor() const
{
	return professor;
}

// getters e setters
void Curso::setProfessor(Professor professor)
{
	this->professor = professor;
}

void Curso::adicionaAluno(Aluno aluno) // adiciona um aluno matriculado no curso
{
	alunosMatriculados.push_back(aluno);
}

void Curso::removeAluno(Aluno aluno) // remove um aluno matriculado no curso
{
	for (int i = 0; i < (int)alunosMatriculados.size(); i++) // percorre o vetor de alunos matriculados
	{
		if (alunosMatriculados[i].getMatricula() == aluno.getMatricula())
		{
			alunosMatriculados.erase(alunosMatriculados.begin() + i); // remove o aluno do vetor
			break;
		}
	}
}

vector<Aluno> Curso::getAlunosMatriculados() const // retorna os alunos matriculados no curso
{
	return alunosMatriculados;
}

bool Curso::isAlocado() const // retorna se o curso ja foi alocado em uma sala duas vezes por semana
{
	return alocado;
}

void Curso::setAlocado(bool alocado) // seta se o curso ja foi alocado em uma sala duas vezes por semana
{
	this->alocado = alocado;
}

vector<int> Curso::getDiasDaSemana() const // retorna os dias da semana em que o curso e ministrado
{
	return diasDaSemana;
}

void Curso::adicionaDiaDaSemana(int diaDaSemana) // adiciona um dia da semana em que o curso e ministrado
{
	diasDaSemana.push_back(diaDaSemana);
}