#ifndef __PESSOA_H_
#define __PESSOA_H_

#include <string>
using namespace std;

class Pessoa
{
	string nome, telefone, cpf;

public:
	Pessoa(string nome, string telefone, string cpf);	// construtor de 3 parametros (nome, telefone e cpf)
	Pessoa();	// construtor de 0 parametros
	~Pessoa();										// destrutor
	// getters e setters
	virtual string getNome() const;				
	virtual string getTelefone() const;
	virtual string getCpf() const;
	virtual void setNome(string);
	virtual void setTelefone(string);
	virtual void setCpf(string);
};

#endif