#ifndef __MENU_H_
#define __MENU_H_

#include "crud-aluno.h"
#include "crud-professor.h"
#include "crud-curso.h"
#include "crud-gestor.h"
#include "crud-sala.h"
#include "alocar-cursos.h"
#include "relatorio.h"

using namespace std;

class Menu
{
	int sessao, opcao;			  // sessao: 1 = aluno, 2 = professor, 3 = gestor
								  // opcao: opcao do menu
	CrudAluno &crudAluno;		  // referencia para a classe CrudAluno
	CrudProfessor &crudProfessor; // referencia para a classe CrudProfessor
	CrudGestor &crudGestor;		  // referencia para a classe CrudGestor
	CrudCurso &crudCurso;		  // referencia para a classe CrudCurso
	CrudSala &crudSala;			  // referencia para a classe CrudSala
	AlocarCursos &alocarCursos;	  // referencia para a classe AlocarCursos
	Relatorio &relatorio;		  // referencia para a classe Relatorio

	// metodos privados
	void exibeMenuInicial();   // exibe o menu inicial
	void gerenciarAluno();	   // gerencia o aluno
	void gerenciarProfessor(); // gerencia o professor
	void gerenciarGestor();	   // gerencia o gestor
	void gerenciarCurso();	   // gerencia o curso
	void gerenciarSala();	   // gerencia a sala
	void gerarRelatorio();	   // gera o relatorio

public:
	// construtor de 7 parametros (referencias para as classes CrudAluno, CrudProfessor, CrudGestor, CrudCurso, CrudSala, AlocarCursos e Relatorio)
	Menu(
		CrudAluno &crudAluno,
		CrudProfessor &crudProfessor,
		CrudGestor &crudGestor,
		CrudCurso &crudCurso,
		CrudSala &crudSala,
		AlocarCursos &alocarCursos,
		Relatorio &relatrio);

	~Menu(); // destrutor

	void login(); // metodo publico login
};

#endif