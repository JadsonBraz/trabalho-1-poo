#include "pessoa.h"

// construtor de 3 parametros (nome, telefone e cpf)
Pessoa::Pessoa(string nome, string telefone, string cpf) : nome(nome), telefone(telefone), cpf(cpf) {}

Pessoa::Pessoa() {}
Pessoa::~Pessoa() {} // destrutor

// getters e setters
string Pessoa::getNome() const
{
	return nome;
}

string Pessoa::getTelefone() const
{
	return telefone;
}

string Pessoa::getCpf() const
{
	return cpf;
}

void Pessoa::setNome(string nome)
{
	this->nome = nome;
}

void Pessoa::setTelefone(string telefone)
{
	this->telefone = telefone;
}

void Pessoa::setCpf(string cpf)
{
	this->cpf = cpf;
}
