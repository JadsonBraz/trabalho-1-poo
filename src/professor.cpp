#include "professor.h"

// construtor de 4 parametros (nome, telefone, cpf e codCurso)
Professor::Professor(string nome, string telefone, string cpf, int codCurso) : Pessoa(nome, telefone, cpf), codCurso(codCurso) {}

// construtor de 3 parametros (nome, telefone e cpf)
Professor::Professor(string nome, string telefone, string cpf) : Pessoa(nome, telefone, cpf) {}

Professor::Professor() {} // construtor de 0 parametros
Professor::~Professor() {} // destrutor

int Professor::getCodCurso() { return codCurso; } // getters e setters

void Professor::setCodCurso(int codCurso) { this->codCurso = codCurso; } // getters e setters