#ifndef __DADOS_H_
#define __DADOS_H_

#include <vector>
#include "gestor.h"
#include "professor.h"
#include "aluno.h"
#include "curso.h"
#include "sala.h"

// TODO: ler dados de um txt e criar um vetor de professores

// cadastrar 1 gestor:
Gestor gestor1("Joao", "(92) 99544-8197", "730.536.610-20", "joao", "123");

// cadastrar 4 salas:
Sala sala1(1, 10);
Sala sala2(2, 5);
Sala sala3(3, 5);
Sala sala4(4, 3);

// cadastrar 8 professores:
Professor professor1("Joao", "(92) 99544-8197", "730.536.610-20");
Professor professor2("Maria", "(92) 99544-8197", "312.536.610-20");
Professor professor3("Jose", "(92) 99544-8197", "912.536.610-20");
Professor professor4("Tamara", "(92) 99544-8197", "712.536.610-20");
Professor professor5("Cleber", "(92) 99544-8197", "132.536.610-20");
Professor professor6("Guilherme", "(92) 99544-8197", "332.536.610-20");
Professor professor7("Dora", "(92) 99544-8197", "432.536.610-20");
Professor professor8("Fatima", "(92) 99544-8197", "532.536.610-20");

// cadastrar 8 alunos:
Aluno aluno1("Jane Teixeira Alentejo", "(92) 97941-3442", "842.532.461-09", 1);
Aluno aluno2("Emanoel de Rodrigues Gabrig", "(92) 98918-3620", "623.496.596-23", 2);
Aluno aluno3("Tomás Caldas Marcello", "(92) 99696-0734", "655.637.082-76", 3);
Aluno aluno4("Jocilea Moreira Dutra", "(92) 98748-9276", "854.314.212-12", 4);
Aluno aluno5("Katyene Sarmanto Damasceno", "(92) 99343-0240", "853.215.251-10", 5);
Aluno aluno6("Adriana Vitorino Zuniga", "(92) 98103-5751", "864.142.821-62", 6);
Aluno aluno7("Fabiana Stutz Moura", "(92) 99179-1463", "518.537.512-43", 7);
Aluno aluno8("Honoria Carmo Freitas", "(92) 98498-9872", "345.357.483-49", 8);

// cadastrar 8 cursos:
Curso curso1("Selecao de Materiais", professor1);
Curso curso2("Mecanica dos Fluidos", professor2);
Curso curso3("Arquitetura de Computadores", professor3);
Curso curso4("Estrutura de Dados", professor4);
Curso curso5("Fisica", professor5);
Curso curso6("Calculo", professor6);
Curso curso7("Algebra Linear", professor7);
Curso curso8("Estatistica", professor8);

// criar vetores de gestores, professores, alunos, salas e cursos:
vector<Gestor> gestores = {gestor1};
vector<Professor> professores = {professor1, professor2, professor3, professor4, professor5, professor6, professor7, professor8};
vector<Aluno> alunos = {aluno1, aluno2, aluno3, aluno4, aluno5, aluno6, aluno7, aluno8};
vector<Sala> salas = {sala1, sala2, sala3, sala4};
vector<Curso> cursos = {curso1, curso2, curso3, curso4, curso5, curso6, curso7, curso8};

#endif