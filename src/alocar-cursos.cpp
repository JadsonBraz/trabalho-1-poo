#include "alocar-cursos.h"
#include <iomanip>

AlocarCursos::AlocarCursos(vector<Sala> salas, vector<Curso> cursos) : salas(salas), cursos(cursos) {} // construtor de dois parametros: salas e cursos cadastrados

AlocarCursos::AlocarCursos() : salas(vector<Sala>()), cursos(vector<Curso>()) {} // construtor vazio

AlocarCursos::~AlocarCursos() {} // destrutor

void AlocarCursos::alocar()
{
	// organizar salas por capacidade
	sort(salas.begin(), salas.end(), [](Sala a, Sala b)
		 { return a.getCapacidade() > b.getCapacidade(); });
	// organizar cursos por numero de alunos matriculados
	sort(cursos.begin(), cursos.end(), [](Curso a, Curso b)
		 { return a.getAlunosMatriculados().size() > b.getAlunosMatriculados().size(); });

	// percorrer o vetor de salas
	for (int j = 0; j < (int)salas.size(); j++)
	{
		// percorrer o vetor de cursos
		for (int i = 0; i < (int)cursos.size(); i++)
		{
			/**
			 * se a sala ja tiver 5 cursos (segunda a sexta)
			 * ou a sala nao tiver capacidade para o curso
			 * ou o curso ja estiver alocado duas vezes na semana
			 * entao pula para o proximo curso
			 */
			if (salas.at(j).getCursos().size() >= 5 || salas.at(j).getCapacidade() < (int)cursos.at(i).getAlunosMatriculados().size() || cursos.at(i).isAlocado())
				continue;

			/**
			 * se o curso ja tiver um dia da semana alocado
			 * e o dia da semana for igual ao dia da semana da sala
			 * entao pula para o proximo curso
			 */
			if (cursos.at(i).getDiasDaSemana().size() > 0)
			{
				vector<int> diasDaSemana = cursos.at(i).getDiasDaSemana();
				int diaDaSemana = diasDaSemana.at(0);

				if (diaDaSemana == ((int)salas.at(j).getCursos().size() - 1))
				{
					continue;
				}
			}

			// senao, adiciona o dia da semana no vetor de dias da semana do curso
			cursos.at(i).adicionaDiaDaSemana((int)salas.at(j).getCursos().size() - 1);
			// adiciona o codigo do curso no vetor de cursos da sala
			salas.at(j).adicionaCurso(cursos.at(i));
			// cout << "Curso " << i << " alocado na sala " << j << endl;	// DEBUG

			// se o curso tiver dois dias da semana alocados
			// entao seta o atributo alocado como true
			if (cursos.at(i).getDiasDaSemana().size() == 2)
				cursos.at(i).setAlocado(true);
		}
	}
}

void AlocarCursos::verAlocacao()
{
	// vetor com os dias da semana
	vector<string> diasDaSemana = {"Segunda", "Terça", "Quarta", "Quinta", "Sexta"};

	// imprime a tabela de alocacao percorrendo o vetor de salas
	cout << setw(30) << left << "Sala";
	for (string dia : diasDaSemana)
	{
		cout << setw(30) << left << dia;
	}

	cout << endl;

	for (int i = 0; i < (int)salas.size(); i++)
	{
		cout << left << "Sala " << salas.at(i).getCodigo() << " - " << salas.at(i).getCapacidade() << " alunos"
			 << "\t";
		for (int j = 0; j < (int)salas.at(i).getCursos().size(); j++)
		{
			for (int k = 0; k < (int)cursos.size(); k++)
			{
				if (cursos.at(k).getNome() == salas.at(i).getCursos().at(j).getNome())
				{
					cout << setw(30) << left << cursos.at(k).getNome();
				}
			}
		}
		cout << endl;
	}
}