#ifndef __ALUNO_H_
#define __ALUNO_H_

#include "pessoa.h"

class Aluno : public Pessoa
{
	int matricula; // matricula e codigo do curso

public:
	// construtor de cinco parametros: nome, telefone, cpf, matricula e codigo do curso:
	Aluno(string nome, string telefone, string cpf, int matricula);
	~Aluno();				  // destrutor
	int getMatricula() const; // funcao que retorna a matricula do aluno (constante: somente leitura)
	void setMatricula(int);	  // funcao que altera a matricula do aluno
};

#endif