#ifndef __CRUD_PROFESSOR_H_
#define __CRUD_PROFESSOR_H_

#include "professor.h"
#include "curso.h"
#include <vector>
#include <iostream>
using namespace std;

class CrudProfessor
{
	vector<Professor> &professores; // vetor de todos os professores cadastrados
	vector<Curso> &cursos;			// vetor de todos os cursos cadastrados

protected:
	// dados utilizados no input de dados do usuario
	string nome, telefone, cpf;
	int codCurso;
	bool professorExiste = false;

public:
	// crud de dois parametros: professores e cursos cadastrados
	CrudProfessor(vector<Professor> &professores, vector<Curso> &cursos);
	~CrudProfessor(); // destrutor
	bool criar();	  // funcao que cria um novo professor
	void ler();		  // funcao que pesquisa professores cadastrados
	void editar();	  // funcao que edita os dados de um professor
	void remover();	  // funcao que remove um professor
};

#endif