#include "menu.h"

// construtor
Menu::Menu(
	CrudAluno &crudAluno, CrudProfessor &crudProfessor, CrudGestor &crudGestor, CrudCurso &crudCurso, CrudSala &crudSala, AlocarCursos &alocarCursos, Relatorio &relatorio)
	: crudAluno(crudAluno), crudProfessor(crudProfessor), crudGestor(crudGestor), crudCurso(crudCurso), crudSala(crudSala), alocarCursos(alocarCursos), relatorio(relatorio)
{
	this->sessao = 0;
}

Menu::~Menu() {} // destrutor

// metodo que exibe o menu inicial (login)
void Menu::login()
{
	while (1)
	{
		string usuario, senha;	   // usuario e senha para opcao gestor
		bool gestorLogado = false; // variavel para verificar se o gestor esta logado

		cout << "Escolha uma opção:" << endl;
		cout << "1 - Sou gestor" << endl;
		cout << "2 - Sou professor" << endl;
		cout << "3 - Sou aluno" << endl;
		cout << "4 - SAIR" << endl;
		cout << "> ";
		cin >> opcao;

		system("clear"); // limpa a tela
		// middleware
		switch (opcao)
		{
		case 1:
			gestorLogado = crudGestor.autenticar(); // chama o metodo autenticar da classe CrudGestor
			if (gestorLogado)
			{
				sessao = 1; // sessao = 1 significa que o usuario eh gestor
			}
			else
			{
				cout << "Usuário ou senha inválidos!" << endl
					 << endl;
			}
			break;

		case 2:
			system("clear");
			cout << "Bem vindo, professor" << endl; // professor loga sem usuario e senha
			sessao = 2;								// sessao = 2 significa que o usuario eh professor
			break;

		case 3:
			system("clear");
			cout << "Bem vindo, aluno" << endl; // aluno loga sem usuario e senha
			sessao = 3;							// sessao = 3 significa que o usuario eh aluno
			break;

		case 4:
			cout << "Saindo..." << endl;
			exit(0); // sai do programa
			break;

		default:
			cout << "Opção inválida!" << endl; // opcao invalida
		}

		if (sessao) // se sessao for diferente de 0, chama o metodo exibeMenuInicial
		{
			exibeMenuInicial();
		}
	}
}

// metodo que exibe o menu inicial
void Menu::exibeMenuInicial()
{
	while (1)
	{
		cout << endl;
		cout << "Escolha uma opção:" << endl;
		cout << "1 - Gerenciar Aluno" << endl;
		cout << "2 - Gerenciar Professor" << endl;
		cout << "3 - Gerenciar Curso" << endl;
		cout << "4 - Gerenciar Gestor" << endl;
		cout << "5 - Gerenciar Sala" << endl;
		cout << "6 - Gerar relatório" << endl;
		cout << "7 - SAIR" << endl;
		cout << "> ";
		cin >> opcao;

		system("clear");
		switch (opcao)
		{
		case 1:
			gerenciarAluno(); // chama o metodo gerenciarAluno
			break;
		case 2:
			gerenciarProfessor(); // chama o metodo gerenciarProfessor
			break;
		case 3:
			gerenciarCurso(); // chama o metodo gerenciarCurso
			break;
		case 4:
			gerenciarGestor(); // chama o metodo gerenciarGestor
			break;
		case 5:
			gerenciarSala(); // chama o metodo gerenciarSala
			break;
		case 6:
			gerarRelatorio(); // chama o metodo gerarRelatorio
			break;
		case 7:
			cout << "Saindo..." << endl;
			exit(0); // sai do programa
			break;
		default:
			cout << "Opção inválida!" << endl;
		}
	}
}

// metodo que exibe o menu de gerenciamento de aluno
void Menu::gerenciarAluno()
{
	while (opcao != 5) // enquanto opcao for diferente de 5, exibe o menu de gerenciamento de aluno
	{
		cout << endl;
		cout << "Escolha uma opção:" << endl;
		cout << "1 - Cadastrar Aluno" << endl;
		cout << "2 - Consultar Alunos" << endl;
		cout << "3 - Remover Aluno" << endl;
		cout << "4 - Editar Aluno" << endl;
		cout << "5 - Voltar" << endl;
		cout << "> ";
		cin >> opcao;

		system("clear");
		if (opcao == 1 && sessao == 1)
		{

			bool alunoCriado = crudAluno.criar(); // chama o metodo criar da classe CrudAluno
			if (alunoCriado)					  // se o aluno for criado, chama o metodo alocar da classe AlocarCursos
			{
				alocarCursos.alocar(); // chama o metodo alocar da classe AlocarCursos
				cout << endl
					 << "Cursos realocados:" << endl;
				alocarCursos.verAlocacao(); // chama o metodo verAlocacao da classe AlocarCursos para mostrar a realocacao
			}
		}
		else if (opcao == 2)
		{
			crudAluno.ler(); // chama o metodo ler da classe CrudAluno
		}
		else if (opcao == 3 && sessao == 1)
		{
			crudAluno.remover(); // chama o metodo remover da classe CrudAluno
		}
		else if (opcao == 4 && sessao == 1)
		{
			crudAluno.editar(); // chama o metodo editar da classe CrudAluno
		}
		else if (opcao == 5)
		{
			break;
		}
		else
		{
			cout << "Opção inválida!" << endl;
		}
	}
}

void Menu::gerenciarProfessor()
{
	while (opcao != 5) // enquanto a opcao for diferente de 5, exibe o menu de gerenciamento de professor
	{
		cout << endl;
		cout << "Escolha uma opção:" << endl;
		cout << "1 - Cadastrar Professor" << endl;
		cout << "2 - Consultar Professores" << endl;
		cout << "3 - Remover Professor" << endl;
		cout << "4 - Editar Professor" << endl;
		cout << "5 - Voltar" << endl;
		cout << "> ";
		cin >> opcao;

		system("clear");
		if (opcao == 1 && sessao == 1)
		{
			crudProfessor.criar(); // chama o metodo criar da classe CrudProfessor
		}
		else if (opcao == 2)
		{
			crudProfessor.ler(); // chama o metodo ler da classe CrudProfessor
		}
		else if (opcao == 3 && sessao == 1)
		{
			crudProfessor.remover(); // chama o metodo remover da classe CrudProfessor
		}
		else if (opcao == 4 && sessao == 1)
		{
			crudProfessor.editar(); // chama o metodo editar da classe CrudProfessor
		}
		else if (opcao == 5)
		{
			break;
		}
		else
		{
			cout << "Opção inválida!" << endl;
		}
	}
}

void Menu::gerenciarCurso()
{
	while (opcao != 6) // enquanto a opcao for diferente de 6, o menu de gerenciamento de curso sera exibido
	{
		cout << endl;
		cout << "Escolha uma opção:" << endl;
		cout << "1 - Cadastrar Curso" << endl;
		cout << "2 - Consultar Cursos" << endl;
		cout << "3 - Remover Curso" << endl;
		cout << "4 - Editar Curso" << endl;
		cout << "5 - Consultar alocação" << endl;
		cout << "6 - Voltar" << endl;
		cout << "> ";
		cin >> opcao;

		system("clear");
		if (opcao == 1 && sessao == 1)
		{
			bool cursoCriado = crudCurso.criar(); // chama o metodo criar da classe CrudCurso
			if (cursoCriado)					  // se o curso for criado, chama o metodo alocar da classe AlocarCursos
				alocarCursos.alocar();			  // chama o metodo alocar da classe AlocarCursos
		}
		else if (opcao == 2)
		{
			crudCurso.ler(); // chama o metodo ler da classe CrudCurso
		}
		else if (opcao == 3 && sessao == 1)
		{
			crudCurso.remover(); // chama o metodo remover da classe CrudCurso
		}
		else if (opcao == 4 && sessao == 1)
		{
			crudCurso.editar(); // chama o metodo editar da classe CrudCurso
		}
		else if (opcao == 5)
		{
			alocarCursos.verAlocacao(); // chama o metodo verAlocacao da classe AlocarCursos
		}
		else if (opcao == 6)
		{
			break;
		}
		else
		{
			cout << "Opção inválida!" << endl;
		}
	}
}

void Menu::gerenciarGestor()
{
	system("clear");
	if (sessao == 1)
	{
		crudGestor.cadastrar(); // chama o metodo cadastrar da classe CrudGestor
	}
	else
	{
		cout << "Acesso negado!" << endl; // se a sessao nao for de um gestor, exibe a mensagem de acesso negado
	}
}

void Menu::gerenciarSala()
{
	system("clear");
	if (sessao == 1)
	{
		bool salaCadastrada = crudSala.cadastrar(); // chama o metodo cadastrar da classe CrudSala
		if (salaCadastrada)							// se a sala for cadastrada, chama o metodo alocar da classe AlocarCursos
			alocarCursos.alocar();					// chama o metodo alocar da classe AlocarCursos
	}
	else
	{
		cout << "Acesso negado!" << endl;
	}
}

void Menu::gerarRelatorio()
{
	opcao = 0;
	while (opcao != 5)
	{
		cout << endl;
		cout << "Escolha uma opção:" << endl;
		cout << "1 - Relatório de Alunos" << endl;
		cout << "2 - Relatório de Professores" << endl;
		cout << "3 - Relatório de Cursos" << endl;
		cout << "4 - Relatório de Salas" << endl;
		cout << "5 - Voltar" << endl;
		cout << "> ";
		cin >> opcao;

		system("clear");

		if (sessao == 1) // se a sessao for de um gestor, exibe o menu de geracao de relatorio
		{
			switch (opcao)
			{
			case 1:
				relatorio.relatorioAlunos(); // chama o metodo relatorioAlunos da classe Relatorio
				break;
			case 2:
				relatorio.relatorioProfessores(); // chama o metodo relatorioProfessores da classe Relatorio
				break;
			case 3:
				relatorio.relatorioCursos(); // chama o metodo relatorioCursos da classe Relatorio
				break;
			case 4:
				relatorio.relatorioSalas(); // chama o metodo relatorioSalas da classe Relatorio
				break;
			case 5:
				break;
			default:
				cout << "Opção inválida!" << endl; // se a opcao for invalida, exibe a mensagem de opcao invalida
			}
		}
		else
		{
			if (opcao != 5)
				cout << "Acesso negado!" << endl;
		}
	}
}
