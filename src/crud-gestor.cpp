#include "crud-gestor.h"
// construtor de um parametro: vetor de gestores
CrudGestor::CrudGestor(vector<Gestor> &gestores) : gestores(gestores) {}
// destrutor
CrudGestor::~CrudGestor() {}

// funcao que autentica um gestor
bool CrudGestor::autenticar()
{
	cout << "\nDigite seu usuario: > ";
	cin >> usuario;
	cout << "Digite sua senha: > ";
	cin >> senha;

	for (int i = 0; i < (int)gestores.size(); i++)
	{
		if (gestores[i].autenticar(usuario, senha))
		{
			system("clear");
			cout << "Bem vindo, Gestor " << gestores[i].getNome() << endl; // mostra nome do gestor
			return true;												   // retorna true para indicar que o gestor foi autenticado
		}
	}
	return false; // retorna false para indicar que o gestor nao foi autenticado
}

// funcao que cadastra um novo gestor
void CrudGestor::cadastrar()
{
	cout << "Cadastrar gestor" << endl;

	cout << "Digite o nome do gestor: > ";
	cin >> nome;
	cout << "Digite o cpf do gestor: > ";
	cin >> cpf;
	// verifica se o cpf ja esta cadastrado
	for (int i = 0; i < (int)gestores.size(); i++)
	{
		if (gestores[i].getCpf() == cpf)
		{
			cout << "Gestor já cadastrado!" << endl;
			return; // retorna para o menu caso o cpf ja esteja cadastrado
		}
	}
	cout << "Digite o telefone do gestor: > ";
	cin >> telefone;
	cout << "Digite o usuario do gestor: > ";
	cin >> usuario;
	// verifica se o usuario ja esta cadastrado
	for (int i = 0; i < (int)gestores.size(); i++)
	{
		if (gestores[i].getUsuario() == usuario)
		{
			cout << "Usuario já cadastrado!" << endl;
			return; // retorna para o menu caso o usuario ja esteja cadastrado
		}
	}
	cout << "Digite a senha do gestor: > ";
	cin >> senha;

	Gestor gestor(nome, telefone, cpf, usuario, senha); // cria gestor
	gestores.push_back(gestor);							// adiciona gestor ao vetor de gestores
	cout << "Gestor cadastrado com sucesso!" << endl;
}