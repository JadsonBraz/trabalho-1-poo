#ifndef __CURSO_H_
#define __CURSO_H_

#include <vector>
#include <string>
#include "professor.h"
#include "aluno.h"
using namespace std;

class Curso
{
	string nome;					  // nome do curso
	Professor professor;			  // objeto do tipo professor
	vector<int> diasDaSemana;		  // vetor de dias da semana em que o curso e ministrado
	vector<Aluno> alunosMatriculados; // vetor de alunos matriculados no curso
	bool alocado;					  // indica se o curso ja foi alocado em uma sala

public:
	Curso(string nome, Professor professor); // construtor de tres parametros (nome, codigo e cpf do professor)
	Curso(string nome);						 // construtor de dois parametros (nome e codigo)
	~Curso();								 // destrutor
	// getters e setters
	string getNome() const;
	void setNome(string);
	Professor getProfessor() const;
	void setProfessor(Professor);
	void adicionaAluno(Aluno aluno);
	void removeAluno(Aluno aluno);
	vector<Aluno> getAlunosMatriculados() const; // retorna o numero de alunos matriculados no curso
	bool isAlocado() const;						 // retorna se o curso ja foi alocado em uma sala duas vezes por semana
	void setAlocado(bool alocado);				 // seta se o curso ja foi alocado em uma sala duas vezes por semana
	vector<int> getDiasDaSemana() const;		 // retorna os dias da semana em que o curso e ministrado
	void adicionaDiaDaSemana(int diaDaSemana);	 // adiciona um dia da semana em que o curso e ministrado
};

#endif