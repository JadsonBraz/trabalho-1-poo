#ifndef __CRUD_CURSO_H_
#define __CRUD_CURSO_H_

#include "curso.h"
#include "professor.h"
#include <vector>
#include <iostream>
using namespace std;

class CrudCurso
{
	vector<Curso> &cursos; // vetor de todos os cursos cadastrados
	vector<Professor> professores; // objeto para acessar as funcoes de crud de professor

protected:
	// dados utilizados no input de dados do usuario
	string nome, cpfProfessor;
	bool cursoExiste = false;
	Professor professor;

public:
	// crud de um parametro: cursos cadastrados
	CrudCurso(vector<Curso> &cursos, vector<Professor> &professor);
	~CrudCurso();	// destrutor
	bool criar();	// funcao que cria um novo curso
	void ler();		// funcao que pesquisa cursos cadastrados
	void editar();	// funcao que edita os dados de um curso
	void remover(); // funcao que remove um curso
};

#endif