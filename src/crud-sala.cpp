#include "crud-sala.h"

CrudSala::CrudSala(vector<Sala> &salas) : salas(salas) {} // construtor da classe CrudSala

CrudSala::~CrudSala() {} // destrutor

bool CrudSala::cadastrar() // funcao que cadastra uma nova sala
{
	cout << "Cadastrar sala" << endl;

	cout << "Digite o código da sala: ";
	cin >> codigo;
	cout << "Digite a capacidade da sala: ";
	cin >> capacidade;

	for (int i = 0; i < (int)salas.size(); i++)
	{
		if (salas[i].getCodigo() == codigo) // verifica se a sala ja esta cadastrada
		{
			cout << "Sala já cadastrada!" << endl;
			return false; // retorna false para indicar que a sala nao foi cadastrada
		}
	}

	Sala sala(codigo, capacidade); // cria um objeto do tipo Sala
	salas.push_back(sala);		   // adiciona o objeto sala ao vetor de salas
	cout << "Sala cadastrada com sucesso!" << endl;

	return true; // retorna true para indicar que a sala foi cadastrada
}