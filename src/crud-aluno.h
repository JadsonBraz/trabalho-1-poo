#ifndef __CRUD_ALUNO_H_
#define __CRUD_ALUNO_H_

#include "aluno.h"
#include "curso.h"
#include <vector>
#include <iostream>
using namespace std;

class CrudAluno
{
	vector<Aluno> &alunos; // vetor de todos os alunos cadastrados
	vector<Curso> &cursos; // vetor de todos os cursos cadastrados

protected:
	// dados utilizados no input de dados do usuario
	string nome, telefone, cpf;
	int codCurso, matricula;
	bool alunoExiste = false;

public:
	// crud de dois parametros: alunos e cursos cadastrados
	CrudAluno(vector<Aluno> &alunos, vector<Curso> &cursos);
	~CrudAluno();	// destrutor
	bool criar();	// funcao que cria um novo aluno
	void ler();		// funcao que pesquisa alunos cadastrados
	void editar();	// funcao que edita os dados de um aluno
	void remover(); // funcao que remove um aluno
};

#endif