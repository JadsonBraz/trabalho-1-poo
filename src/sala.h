#ifndef __SALA_H_
#define __SALA_H_

#include "curso.h"
#include <vector>

class Sala
{
	// codigo da sala (1, 2, 3, ...)
	// capacidade maxima de alunos na sala
	int codigo, capacidade;

	// codigos dos cursos que estao alocados na sala
	vector<Curso> cursos;

public:
	// construtor de 2 parametros (codigo e capacidade)
	Sala(int codigo, int capacidade);
	// destrutor
	~Sala();
	// getters
	int getCapacidade() const;
	int getCodigo() const;
	vector<Curso> getCursos() const;

	// adicionar curso alocado na sala
	void adicionaCurso(Curso);
};

#endif